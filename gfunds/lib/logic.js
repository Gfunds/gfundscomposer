/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const NS = 'gowdanar.gfunds';

/**
 * Write your transction processor functions here
 */

/**
 * DocumentVerification transaction
 * @param {gowdanar.gfunds.DocumentVerification} tx
 * @transaction
 */
async function documentVerification(tx) {

    // Get asset registry for MemberDoc
    const memberDocRegistry = await getAssetRegistry(NS + '.MemberDoc');
    
    // Get participant registry for Member
    const memberRegistry = await getParticipantRegistry(NS + '.Member');

    // Get participant registry for Foreman
    const foremanRegistry = await getParticipantRegistry(NS + '.Foreman');

    // Get the memberDoc Id from transaction
    const memDocId = tx.memberDocId.getIdentifier();

    // Checking the document with registry
    const mdoc = await memberDocRegistry.get(memDocId);

    // Make sure that document exists
    if (!mdoc) {
        throw new Error(`Document with id ${memDocId} does not exist`);
    }


    // Get foreman ID from transaction
    const foremanId = tx.foremanId.getIdentifier();
	
    // Make sure that Foreman exists
    const foreman = await foremanRegistry.get(foremanId);
    if (!foreman) {
        throw new Error(`Foreman with id ${foreman} does not exist`);
    }
    
    // Storing oldStatus
    let oldStatus = mdoc.foremanVerifystatus;

    // Update Document status
    mdoc.foremanSign = tx.foremanSign;
    mdoc.foremanVerifystatus = tx.foremanStatus;

    // Update the asset in the asset registry.
    await memberDocRegistry.update(mdoc);
    
    // Performing the DocumentVerifyEvent
 	let event = getFactory().newEvent(NS, 'DocumentVerifyEvent');
    event.mDocId = tx.memberDocId;
    event.oldStatus = oldStatus;
    event.newStatus = tx.foremanStatus;
    emit(event);
    
}


/**
 * GfundsAdminVerification transaction
 * @param {gowdanar.gfunds.GfundsAdminVerification} adtx
 * @transaction
 */

async function GfundsAdminVerification(adtx) {
	
  // Get asset registry for MemberDoc
    const memberDocRegistry = await getAssetRegistry(NS + '.MemberDoc');
	
  	//Get GfundsAdmin registry 	
    const GfundsAdminRegistry = await getParticipantRegistry(NS + '.GfundsAdmin');
  	
    // Get the memberDoc Id from transaction
    const memDocId = adtx.memberDocId.getIdentifier();

    // Checking the document with registry
    const mdoc = await memberDocRegistry.get(memDocId);

    // Make sure that document exists
    if (!mdoc) {
        throw new Error(`Document with id ${memDocId} does not exist`);
    }

  	// Make sure that document is verified by Foreman
  	if (mdoc.foremanVerifystatus !== "APPROVED") {
        throw new Error(`Document with id ${memDocId} couldn't verified by the Foreman`);
    }
	
    // Get the Admin Id from transaction
    const gfundsAd = adtx.gfundsAdminId.getIdentifier();
      
  	// Checking the Admin with Registry
    const gAdmin = await GfundsAdminRegistry.get(gfundsAd);
      
  	// Make sure that Admin exist
  	if (!gAdmin) {
        throw new Error(`GfundsAdmin with id ${gfundsAd} does not exist`);
    }
    
  	// Storing old Adminstatus
  	let oldStatus = mdoc.adminVerifyStatus;
  
  	// Update Document Adminstatus
    mdoc.adminVerifyStatus = adtx.adminStatus;
    mdoc.gfundsAdminSign = adtx.adminSign;
  
	// Update the asset in the asset registry.
    await memberDocRegistry.update(mdoc);
  	
  	// Performing the GfundsAdminVerifyEvent
 	let event = getFactory().newEvent(NS, 'GfundsAdminVerifyEvent');
    event.mDocId = adtx.memberDocId;
    event.oldStatus = oldStatus;
    event.newStatus = adtx.adminStatus;
    emit(event);
}